import React, { useState } from "react";

import { BrowserRouter, Switch, Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { TodoList, CompletedTodoList, UncompletedTodoList, ItemsLeft } from "./components/todolist/Todolist";
import { addTodo, clearCompletedTodos } from "./actions/todosActions";
import { Nav } from "./components/nav/Nav";
function App(props) {
  const [inputText, setInputText] = useState("");
  const dispatch = useDispatch();
  const todos = useSelector((state) => state.todos);
  const handleAddToDo = (event) => {
    if (event.which === 13) {
      dispatch(addTodo(inputText));
      setInputText("");
    }
  };

  return (
    <BrowserRouter>
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            onChange={(event) => setInputText(event.target.value)}
            onKeyDown={(event) => handleAddToDo(event)}
            className="new-todo"
            value={inputText}
            placeholder="What needs to be done?"
            autoFocus
          />
        </header>

        <Switch>
          <Route exact path="/" component={TodoList}>
            <TodoList todos={Object.values(todos)} />
          </Route>
          <Route path="/active" component={UncompletedTodoList}>
            <UncompletedTodoList todos={Object.values(todos)} />
          </Route>
          <Route path="/completed" component={CompletedTodoList}>
            <CompletedTodoList todos={Object.values(todos)} />
          </Route>
        </Switch>
        <footer className="footer">
          <span className="todo-count">
            <strong>
              <ItemsLeft todos={Object.values(todos)} />
            </strong>{" "}
            item(s) left
          </span>
          <Nav />
          <button className="clear-completed" onClick={() => dispatch(clearCompletedTodos())}>
            Clear completed
          </button>
        </footer>
      </section>
    </BrowserRouter>
  );
}

export default App;
