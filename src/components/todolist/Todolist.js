import React from "react";

import { TodoItem } from "../todoitem/Todoitem";
export const TodoList = (props) => {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem
            title={todo.title}
            completed={todo.completed}
            id={todo.id}
            handleToggle={props.handleToggle}
            handleDeleteToDo={props.handleDeleteToDo}
          />
        ))}
      </ul>
    </section>
  );
};

export const CompletedTodoList = (props) => {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos
          .filter((todo) => todo.completed === true)
          .map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
              handleToggle={props.handleToggle}
              handleDeleteToDo={props.handleDeleteToDo}
            />
          ))}
      </ul>
    </section>
  );
};

export const UncompletedTodoList = (props) => {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos
          .filter((todo) => todo.completed === false)
          .map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
              handleDeleteToDo={props.handleDeleteToDo}
            />
          ))}
      </ul>
    </section>
  );
};

export const ItemsLeft = (props) => {
  let num = 0;
  for (const i of props.todos) {
    if (i.completed === false) {
      num++;
    }
  }
  return num;
};
